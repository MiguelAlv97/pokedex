//
//  NetworkPokemon.swift
//  Pokedex
//
//  Created by Miguel Esteban Alvarez Naranjo on 19/6/18.
//  Copyright © 2018 Miguel Esteban Alvarez Naranjo. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
// alamofire instalacion
// instalar cocoapods
// en la carpeta del proyecto poner pod init
// en el archivo Podfile añadir una line (ver documentacion)
// ejecturar
// importar Alamofire

// abrir workspace no project
// ejectura command + B para construir todo
class NetworkPokemon{
    func getAllPokemon(completion:@escaping ([Pokemon])->()){
        var pokemonArray: [Pokemon] = []
        let group = DispatchGroup()
        for i in 1 ... 8 {
            group.enter()
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseJSON { response in
                guard let data = response.data else {
                    print("Error")
                    return
                }
                
                guard let pokemon = try? JSONDecoder().decode(Pokemon.self, from: data) else {
                    print("Error decoding Pokemon")
                    return
                }
                
                print("asdfafs ", pokemon.sprites.defaultSprite)
                pokemonArray.append(pokemon)
                group.leave()
            }
            
        }
        group.notify(queue: .main){ //espera a que el grupo este vacio
            completion(pokemonArray)
            
        }
    }
    
    func getPokemonImage(url:String, completionHandler: @escaping(UImage) -> ()){
       
        
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }
        
    }
    
    
    
}
