//
//  PokemonDataViewController.swift
//  Pokedex
//
//  Created by Miguel Esteban Alvarez Naranjo on 20/6/18.
//  Copyright © 2018 Miguel Esteban Alvarez Naranjo. All rights reserved.
//

import UIKit

class PokemonDataViewController: UIViewController {

    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var weightLabel: UILabel!
    
    @IBOutlet weak var heightLabel: UILabel!
    
    
    @IBOutlet weak var imagenImageView: UIImageView!
    
    var pokemon: Pokemon!
    var selectedRow: Int!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text? = pokemon.name
        weightLabel.text? = "Weight:   " + "\(pokemon.weight)"
        heightLabel.text? = "Heigth:   " + "\(pokemon.height)"
        
        let bm = NetworkPokemon()
        bm.getPokemonImage(url: pokemon.sprites.defaultSprite, completionHandler: { (image) in
            DispatchQueue.main.async {
                self.imagenImageView.image = imageR
            }
            
            
        })
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
