//
//  ViewController.swift
//  Pokedex
//
//  Created by Miguel Esteban Alvarez Naranjo on 19/6/18.
//  Copyright © 2018 Miguel Esteban Alvarez Naranjo. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    var pokemon:[Pokemon] = []

    @IBOutlet weak var pokemonTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let network = NetworkPokemon()
        network.getAllPokemon { (pokemonArray) in
            
            self.pokemon = pokemonArray
            self.pokemonTableView.reloadData()
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = pokemon[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toInfoPokemonSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier=="toInfoPokemonSegue"{
            let destination = segue.destination as! PokemonDataViewController
            let selectedRow = pokemonTableView.indexPathForSelectedRow![0]
            destination.pokemon = pokemon[selectedRow.row]
            destination.selectedRow = selectedRow.row
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

